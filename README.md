# Building firmware on an ubuntu system and deploying to a GrovePi:

## References:

- https://www.dexterindustries.com/GrovePi/get-started-with-the-grovepi/updating-firmware/
- https://github.com/DexterInd/GrovePi
- http://inotool.org/

## ----> First, on your Ubuntu system

### Need to have arduino installed:
sudo apt-get install arduino

### Install ino utility (reference: http://inotool.org/)
```cd ~/Documents
git clone git://github.com/amperka/ino.git
cd ino/
sudo make install
sudo pip install virtualenv # if not already installed
virtualenv -p /usr/bin/python py2env  # need to use python2 to support ino
source py2env/bin/activate
pip install ino
sudo apt-get install picocom```

### Get the grovepi firmware  

```cd ~/Documents
git clone https://gitlab.com/fiot/grovepi-firmware-build-deploy.git```

(Alternatively, get these files from GrovePi, [here](https://github.com/DexterInd/GrovePi/tree/master/Firmware/Source/v1.2/grove_pi_v1_2_7))

### Clean any previous builds if necessary
```ino clean```

### Build the firmware
```ino build```

### Put the resultant .hex file on the raspberry pi

(Note: the firmware file is in a hidden directory, '.build')

```scp .build/uno/firmware.hex pi@dex.local:```

## ------> Now, on your Pi

### Upload the firmware on the GrovePi microcontroller

``` avrdude -c gpio -p m328p -U flash:w:.build/uno/firmware.hex```
